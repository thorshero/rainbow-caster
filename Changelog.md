# Full Changelog

## Release 0.5 (beta)

- Localization! Now available in English and German
- New About Form
- Introduced Settings
- New feature: OBS Dynamic Text
- Added Reset Map feature
- A lot of bug fixes and improvements
- Added Installer and Portable installation ZIP file

## Release 0.4 (beta)

- New Layout!
- Added Tabs
- Added Resizable Window and matching responsive Controls
- Added MatchURL Setting
- Added optional MatchNotes section
- Added Status Info
- Added Auto-Update feature
- Minor Fixes and improvements

## Release 0.3 (beta)

- Fixed Markdown Export (Table Format) - See Issue #1
- Added Empty Map Handling (e.g. for BO3 - Matches) - See Issue #2

## Release 0.2 (beta)

- Added Operator Bans (Export)
- Added Auto-Export after final Map
- Added Remove Last Round functionality
- Minor Fixes & Changes (Tab Order)

## Release 0.1 (beta)

- Initial Release
- Implemented most features
- Bugs are to be expected ;)

