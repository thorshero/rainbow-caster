# Rainbow Six Casting Tool

> ***Hinweis / Note:*** *Die Deutsche Beschreibung findest du weiter unten! - German description below!* ([Link](#deutsche-beschreibung))

---

### English Description:

## About the program

**Current Version:** Version 0.5 (beta) ([Details for that version](https://gitlab.com/sthorsten/rainbow-caster/tags/v0.5))

This tools aims to be a help for (German) Casters, who regularly cast and stream Rainbow Six Siege games.
This tool can be used by players to get an overview over current games as well!

## Main features:

Collecting and saving Match Data, specifically

- Match title
- Match URL (if available)
- Team names
- Played maps
- More Notes, e.g. playday
- Operator bans for each map
- Round data for each single round:
  - Selected bombspot
  - Team, which has won the round
  - Defuser plants and Counter-Defuses
  - Addtional round information: Matchpoint, Side Switch, Rehost
- Automatic saving match data to text files, which can be read by OBS:
  - Match title
  - Match subtitle (currently what is set in the match notes)
  - Next map (e.g. for pause screens)
  - Game Score (for both teams)

Each Map in the Proleague / ESL / DeSBL Map pool can be used. 

After finishing the match, the collected match data can be exported as markdown file ([Wikipedia](https://en.wikipedia.org/wiki/Markdown)) (which is basically a formatted text file). To open the markdown file, a special editor has to be used. I recommend using Typora ([Download](https://typora.io/)), which i personally use, too. 

Additionally, the match data can be exported as CSV file as well, for example to be opened and edited in Microsoft Excel or other programs supporting CSV files.

## Video

> *There will be a video about using this program and its main feature once feature complete.*

## Download

The program can be downloaded by using the included [Installer](RainbowSixCastingToolSetup.exe) or [ZIP file](RainbowCastingToolPortable.zip). The installer will automatically install the program to the desired location on your computer, sets start menu entrys and so on. The ZIP file can be used for a portable installation and extracted to a location on your computer you like.

> ***Important note:*** *The program is not signed, because of which Windows may show you a "SmartScreen" warning at first startup. The program does not contain any malicious code, but I do not grant any kind of warranty for this program!* 

## Screenshots

Here are some screenshots of the program in action:

![Screenshot1](Screenshots/Screenshot4.png)

![Screenshot2](Screenshots/Screenshot5.png)

![Screenshot3](Screenshots/Screenshot6.png)

## Changelog (Version 0.5)

- Localization! Now available in English and German
- New About Form
- Introduced Settings
- New feature: OBS Dynamic Text
- Added Reset Map feature
- A lot of bug fixes and improvements
- Added Installer and Portable installation ZIP file

*The full Changelog can be found [here](Changelog.md).*

## License

Licensed under the GNU GPLv3 License (See [LICENSE](LICENSE) file for further details)

---

### Deutsche Beschreibung

## Über das Programm

**Aktuelle Version:** Version 0.5 (beta) ([Details zur Version](https://gitlab.com/sthorsten/rainbow-caster/tags/v0.5))

Dieses Tool soll eine Hilfe sein für (deutsche) Caster, die regelmäßig Rainbow Six Siege Spiele casten und übertragen. Natürlich kann das Tool auch von Spielern verwendet werden, um eine Übersicht über laufende Matches zu erhalten!

## Hauptfeatures:

Erfassen und eingeben von Matchdaten, darunter

- Titel des Matches
- Match-URL (falls vorhanden)
- Teamnamen
- Gespielte Maps
- Weitere Notizen, wie Spieltag, o.ä.
- Operator Bans pro Map
- Rundendaten der einzelnen Runden:
  - Gewählter Bombspot
  - Team, welches die Runde gewonnen hat
  - Plant des Defusers und Counter-Defuse
  - Zusätzliche Infos: Matchpoint, Seitenwechsel, Rehost
- Automatisches Updaten von Matchdaten in Textdateien, die z.B. in OBS benutzt werden können:
  - Matchtitel
  - Matchuntertitel (derzeit das, was in Notizen eingetragen ist)
  - Nächste Map (für Pausen-Screens)
  - Spielstand (Score für beide Teams)

Es können alle Maps aus dem DeSBL / ESL Map-Pool verwendet werden.

Nach Beendigung des jeweiligen Spiels kann das Match als Markdown-Datei ([Wikipedia](https://de.wikipedia.org/wiki/Markdown)) exportiert werden (im Grunde eine formatierte Textdatei). Um diese anständig anzeigen zu können wird ein entsprechender Editor benötigt. Für Windows empfehle ich Typora ([Download](https://typora.io/)), diesen benutze ich selbst auch.

Alternativ kann das Match auch als CSV-Datei exportiert werden, um die Daten z.B. mit Microsoft Excel o.ä. öffnen und bearbeiten zu können.

## Video

> *Sobald das Programm Feature-komplett ist, wird hier auch ein Video eingefügt, in welchem die Benutzung und die Hauptfeatures des Programms gezeigt werden.*

## Download

Das Programm kann über den mitgelieferten [Installer](RainbowSixCastingToolSetup.exe) oder über die [ZIP-Datei](RainbowCastingToolPortable.zip) heruntergeladen werden. Der Installer installiert das Programm selbstständig, legt Einträge im Startmenü an, usw. Die ZIP-Datei ist eine portable Installation, die in ein beliebiges Verzeichnis entpackt werden kann.

> ***Wichtiger Hinweis:*** *Das Programm ist nicht signiert, d.h. Windows zeigt eine SmartScreen Warnung beim ersten Ausführen an! Das Programm enthält keinen schädlichen Code, trotzdem wird für das Programm keine Garantie übernommen!*

## Screenshots

So sieht das Programm in Aktion aus:

![Screenshot1](Screenshots/Screenshot1.png)

![Screenshot2](Screenshots/Screenshot2.png)

![Screenshot3](Screenshots/Screenshot3.png)

## Changelog (Version 0.5)

- Localization! Now available in English and German
- New About Form
- Introduced Settings
- New feature: OBS Dynamic Text
- Added Reset Map feature
- A lot of bug fixes and improvements
- Added Installer and Portable installation ZIP file

*Full Changelog can be found [here](Changelog.md).*

## License

Lizensiert unter der GNU GPLv3 License (Siehe [LICENSE](LICENSE) für weitere Details)

---

*This product is not affiliated with Ubisoft in any way.*