namespace RainbowCaster.Operators {
    public class DEFOperators {
        public const string Alibi = "Alibi";
        public const string Bandit = "Bandit";
        public const string Castle = "Castle";
        public const string Caveira = "Caveira";
        public const string Clash = "Clash";
        public const string Doc = "Doc";
        public const string Echo = "Echo";
        public const string Ela = "Ela";
        public const string Frost = "Frost";
        public const string Jäger = "Jäger";
        public const string Kaid = "Kaid";
        public const string Kapkan = "Kapkan";
        public const string Lesion = "Lesion";
        public const string Maestro = "Maestro";
        public const string Mira = "Mira";
        public const string Mozzie = "Mozzie";
        public const string Mute = "Mute";
        public const string Pulse = "Pulse";
        public const string Rook = "Rook";
        public const string Smoke = "Smoke";
        public const string Tachanka = "Tachanka";
        public const string Warden = "Warden";
        public const string Valkyrie = "Valkyrie";
        public const string Vigil = "Vigil";

        public static string[] GetDEFOperators() {
            return new[] {
                Alibi, Bandit, Castle, Caveira, Clash, Doc, Echo, Ela, Frost, Jäger, Kaid, Kapkan, Lesion, Maestro,
                Mira, Mozzie, Mute, Pulse, Rook, Smoke, Tachanka, Warden, Valkyrie, Vigil
            };
        }
    }
}