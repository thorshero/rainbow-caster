namespace RainbowCaster.Operators {
    public class ATKOperators {
        public const string Ash = "Ash";
        public const string Blackbeard = "Blackbeard";
        public const string Blitz = "Blitz";
        public const string Buck = "Buck";
        public const string Capitao = "Capitao";
        public const string Dokkaebi = "Dokkaebi";
        public const string Finka = "Finka";
        public const string Fuze = "Fuze";
        public const string Glaz = "Glaz";
        public const string Gridlock = "Gridlock";
        public const string Hibana = "Hibana";
        public const string IQ = "IQ";
        public const string Jackal = "Jackal";
        public const string Lion = "Lion";
        public const string Maverick = "Maverick";
        public const string Montagne = "Montagne";
        public const string Nokk = "Nokk";
        public const string Nomad = "Nomad";
        public const string Sledge = "Sledge";
        public const string Thatcher = "Thatcher";
        public const string Thermite = "Thermite";
        public const string Twitch = "Twitch";
        public const string Ying = "Ying";
        public const string Zofia = "Zofia";

        public static string[] GetATKOperators() {
            return new[] {
                Ash, Blackbeard, Blitz, Buck, Capitao, Dokkaebi, Finka, Fuze, Glaz, Gridlock, Hibana, IQ, Jackal, Lion,
                Maverick, Montagne, Nokk, Nomad, Sledge, Thatcher, Thermite, Twitch, Ying, Zofia
            };
        }
    }
}