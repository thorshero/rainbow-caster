using System;
using System.Windows.Forms;
using RainbowCaster.Properties;
using RainbowCaster.Properties.strings;

namespace RainbowCaster {
    public partial class AboutForm : Form {
        public AboutForm() {
            InitializeComponent();
        }

        private void AboutForm_Load(object sender, System.EventArgs e) {
            Text = StatusMessages.AboutTheProgramCaption;
            InfoTextLabel.Text = string.Format(main.AboutTheProgramText, Resources.CurrentVersion);
        }

        private void OKButton_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.OK;
        }

        private void GitlabLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/sthorsten/rainbow-caster");
        }
    }
}