using System;
using System.Text;
using RainbowCaster.Maps;

namespace RainbowCaster {
    /*
     * Holds the current Game Session and all Match and Map Data
     */
    public class Session {
        #region Variables

        // Match Base Information
        public string MatchTitle;
        public string MatchURL;
        public string MatchNotes;
       

        public string TeamBlue, TeamOrange;
        public MapData CurrentMap;

        public int BlueMapWins;
        public int OrangeMapWins;

        private int MapCount;
        private MapData[] Maps;

        #endregion

        #region Constructors

        public Session() {
        }

        public Session(string TeamBlue, string TeamOrange) {
            this.TeamBlue = TeamBlue;
            this.TeamOrange = TeamOrange;
        }

        #endregion

        #region Public Methods

        public void SetMatchInformation(string MatchTitle, string MatchURL, string MatchNotes) {
            this.MatchTitle = MatchTitle;
            this.MatchURL = MatchURL;
            this.MatchNotes = MatchNotes;
        }

        public void SetTeamNames(string TeamBlue, string TeamOrange) {
            this.TeamBlue = TeamBlue;
            this.TeamOrange = TeamOrange;
        }

        public void InitMaps(string[] MapNames) {
            MapCount = 1;
            if (!string.IsNullOrEmpty(MapNames[1])) MapCount++;
            if (!string.IsNullOrEmpty(MapNames[2])) MapCount++;
            
            Maps = new MapData[MapCount];

            for (var i = 0; i < MapCount; i++) {
                Maps[i] = new MapData(MapNames[i], i);
            }

            CurrentMap = Maps[0];
        }

        public void ResetCurrentMap() {
            CurrentMap.Reset();
        }

        public string GetNextMap() {
            if (CurrentMap.GetMapIndex() == MapCount - 1) {
                return CurrentMap.GetMapName();
            }

            return Maps[CurrentMap.GetMapIndex() + 1].GetMapName();
        }

        public int GetNextMapIndex() {
            return CurrentMap.GetMapIndex() + 1;
        }
        
        public bool NextMap() {
            int CurrentMapIndex = CurrentMap.GetMapIndex();
            
            //No more Maps
            if (CurrentMapIndex == MapCount - 1)
                return false;

            //Save current Map and select next Map
            Maps[CurrentMapIndex] = CurrentMap;
            CurrentMap = Maps[CurrentMapIndex + 1];

            return true;
        }

        public string ExportAsCSV() {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Titel;" + MatchTitle);
            sb.AppendLine();

            sb.AppendLine("Match-Informationen;");

            //Match URL
            if (!string.IsNullOrEmpty(MatchURL)) {
                sb.AppendLine("Match URL;" + MatchURL);
            }

            //Teams
            sb.AppendLine("Team Blau;" + TeamBlue);
            sb.AppendLine("Team Orange;" + TeamOrange);

            //Maps
            for (int i = 0; i < Maps.Length; i++) {
                sb.AppendLine("Map " + i + ";" + Maps[i].GetMapName());
            }

            //Notes
            if (!string.IsNullOrEmpty(MatchNotes)) {
                string[] notes = MatchNotes.Split(new[] {Environment.NewLine}, StringSplitOptions.None);
                sb.AppendLine("Notizen;" + notes[0]);
                for (int i = 1; i < notes.Length; i++) {
                    sb.AppendLine(";" + notes[i]);
                }
            }

            sb.AppendLine();

            //Export Round Data fore each Map
            for (int i = 0; i < Maps.Length; i++) {
                //Check Round Data
                string export = Maps[i].ExportAsCSV();
                if (!string.IsNullOrEmpty(export)) {
                    sb.AppendLine("Map " + i + ";" + Maps[i].GetMapName());
                    sb.AppendLine("Operator Bans;" + TeamBlue + ";" + TeamOrange);
                    sb.AppendLine(export);
                }
            }

            return sb.ToString();
        }

        public string ExportAsMarkdown() {
            //Create markdown-formatted text for export
            StringBuilder sb = new StringBuilder();

            //Title
            sb.AppendLine("# " + MatchTitle);
            sb.AppendLine("## Match-Informationen");

            //Match URL
            if (!string.IsNullOrEmpty(MatchURL)) {
                sb.AppendLine("\n**Match URL:**");
                sb.AppendLine("- [" + MatchURL + "](" + MatchURL + ")");
            }

            //Teams
            sb.AppendLine("\n**Teams:**");
            sb.AppendLine("- Team Blau: " + TeamBlue);
            sb.AppendLine("- Team Orange: " + TeamOrange);

            //Maps
            sb.AppendLine("\n**Maps:**");
            for (int i = 0; i < Maps.Length; i++) {
                sb.AppendLine("- Map " + i + ": " + Maps[i].GetMapName());
            }

            //Notes
            if (!string.IsNullOrEmpty(MatchNotes)) {
                sb.AppendLine("\n**Weitere Match-Informationen / Notizen:**");
                sb.AppendLine(MatchNotes);
            }

            //Export Round Data fore each Map
            for (int i = 0; i < Maps.Length; i++) {
                //Check Round Data
                string export = Maps[i].ExportRoundsAsMarkdown();
                if (!string.IsNullOrEmpty(export)) {
                    sb.AppendLine("## Map " + i + ": " + Maps[i].GetMapName());
                    sb.AppendLine("**Operator Bans:**");
                    sb.AppendLine("| |" + TeamBlue + "|" + TeamOrange + "|");
                    sb.AppendLine("|-|-|-|");
                    sb.AppendLine(export);
                }
            }

            return sb.ToString();
        }

        #endregion

        #region Private Methods

        #endregion
    }
}