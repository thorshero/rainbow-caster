﻿namespace RainbowCaster.Maps {
    internal class Consulate : BombSpots {
        private const string ArchivesTellers = "B / 1F - Archives / Tellers";
        private const string GarageCafeteria = "B - Garage / Cafeteria";
        private const string LobbyPress = "1F - Lobby / Press Room";
        private const string OfficeMeeting = "2F - Consul Office / Meeting Room";

        override
            public string[] GetAsArray() {
            return new[] {GarageCafeteria, ArchivesTellers, LobbyPress, OfficeMeeting};
        }
    }
}