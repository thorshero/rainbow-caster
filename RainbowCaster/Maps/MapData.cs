﻿using System.Collections.Generic;
using System.Text;

namespace RainbowCaster.Maps {
    public class MapData {

        #region Variables
        
        private readonly string MapName;
        private readonly int MapIndex;
        private BombSpots BombSpots;

        private string[] ATKBans, DEFBans;

        private int RoundCount;
        private List<Round> RoundHistory;
        private int ScoreBlau, ScoreOrange;
        
        #endregion

        public MapData(string MapName, int MapIndex) {
            this.MapName = MapName;
            this.MapIndex = MapIndex;
            ResolveMap();
            RoundCount = 0;
            RoundHistory = new List<Round>();
        }

        #region Public Methods

        public void SetBans(string ATK1, string ATK2, string DEF1, string DEF2) {
            ATKBans = new[] {ATK1, ATK2};
            DEFBans = new[] {DEF1, DEF2};
        }

        public List<string> AddRound(string BombSpot, bool Planted, bool CounterDefused, string Extra, string WinTeam) {
            Round r = new Round {
                Index = ++RoundCount,
                BombSpot = BombSpot,
                Planted = Planted,
                CounterDefused = CounterDefused,
                Extra = Extra,
                WinTeam = WinTeam
            };

            if (r.Extra == null) r.Extra = "";
            
            if (WinTeam == Properties.strings.StatusMessages.Blue) ScoreBlau++;
            if (WinTeam == Properties.strings.StatusMessages.Orange) ScoreOrange++;
            r.Score = ResolveScore();

            RoundHistory.Add(r);
            return GetRoundData(r.Index);
        }

        public void RemoveRound(int Index) {
            Round r = RoundHistory[Index - 1];

            if (r.WinTeam == Properties.strings.StatusMessages.Blue) ScoreBlau--;
            else if (r.WinTeam == Properties.strings.StatusMessages.Orange) ScoreOrange--;

            RoundHistory.Remove(r);
            RoundCount--;
        }

        public int GetRoundCount() {
            return RoundCount;
        }

        public string GetMapName() {
            return MapName;
        }

        public int GetMapIndex() {
            return MapIndex;
        }

        public void Reset() {
            ATKBans = new string[2];
            DEFBans = new string[2];

            RoundCount = 0;
            RoundHistory = new List<Round>();

            ScoreBlau = 0;
            ScoreOrange = 0;
        }

        public string[] GetBombSpots() {
            return BombSpots.GetAsArray();
        }

        public int[] GetScore() {
            return new []{ScoreBlau, ScoreOrange};
        }

        public string ExportRoundsAsMarkdown() {
            if (RoundHistory.Count == 0) return null;

            StringBuilder sb = new StringBuilder();
            
            sb.AppendLine("|**ATK**|" + ATKBans[0] + "|" + ATKBans[1] + "|");
            sb.AppendLine("|**DEF**|" + DEFBans[0] + "|" + DEFBans[1] + "|");

            sb.AppendLine("**Runden:**");
            sb.AppendLine("|Runde|Bomb Spot|Planted|Plant Denied|Extra|Win|Score|");
            sb.AppendLine("|-|-|-|-|-|-|-|");

            for (int i = 1; i <= RoundCount; i++) {
                List<string> rd = GetRoundData(i);
                sb.Append("|");

                foreach (string s in rd) {
                    sb.Append(s + "|");
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }

        public string ExportAsCSV() {
            if (RoundHistory.Count == 0) return null;
            
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("ATK;" + ATKBans[0] + ";" + ATKBans[1] + ";");
            sb.AppendLine("DEF;" + DEFBans[0] + ";" + DEFBans[1] + ";");
            sb.AppendLine("Runde;Bomb Spot;Planted;Plant Denied;Extra;Win;Score;");
            for (int i = 1; i <= RoundCount; i++) {
                List<string> rd = GetRoundData(i);
                foreach (string s in rd) {
                    sb.Append(s + ";");
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }

        #endregion

        #region Private Methods

        private List<string> GetRoundData(int Index) {
            Round r = RoundHistory[Index - 1];

            var Planted = r.Planted ? Properties.strings.StatusMessages.Yes : Properties.strings.StatusMessages.No;
            var CounterDefused = r.CounterDefused ? Properties.strings.StatusMessages.Yes : Properties.strings.StatusMessages.No;

            return new List<string>
                {r.Index.ToString(), r.BombSpot, Planted, CounterDefused, r.Extra, r.WinTeam, r.Score};
        }

        private void ResolveMap() {
            switch (MapName) {
                case "Bank":
                    BombSpots = new Bank();
                    break;
                case "Border":
                    BombSpots = new Border();
                    break;
                case "Clubhouse":
                    BombSpots = new Clubhouse();
                    break;
                case "Coastline":
                    BombSpots = new Coastline();
                    break;
                case "Consulate":
                    BombSpots = new Consulate();
                    break;
                case "Oregon":
                    BombSpots = new Oregon();
                    break;
                case "Villa":
                    BombSpots = new Villa();
                    break;
            }
        }

        private string ResolveScore() {
            return ScoreBlau + " - " + ScoreOrange;
        }

        #endregion

        private class Round {
            public int Index { get; set; }
            public string BombSpot { get; set; }
            public bool Planted { get; set; }
            public bool CounterDefused { get; set; }
            public string Extra { get; set; }
            public string WinTeam { get; set; }
            public string Score { get; set; }
        }
    }
}