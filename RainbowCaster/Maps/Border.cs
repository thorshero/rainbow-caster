﻿namespace RainbowCaster.Maps {
    internal class Border : BombSpots {
        private const string ArmoryArchives = "2F - Armory Lockers / Archives";
        private const string BathroomTellers = "1F - Bathroom / Tellers";
        private const string CustomsInspection = "1F - Customs Inspection / Supply Room";
        private const string VentilationWorkshop = "1F - Ventilation Room / Workshop";

        override
            public string[] GetAsArray() {
            return new[] {VentilationWorkshop, CustomsInspection, BathroomTellers, ArmoryArchives};
        }
    }
}