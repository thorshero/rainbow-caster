﻿namespace RainbowCaster.Maps {
    internal class Villa : BombSpots {
        private const string AviatorGames = "2F Aviator Room / Games Room";
        private const string DiningKitchen = "1F - Dining Room / Kitchen";
        private const string LivingLibrary = "1F - Living Room / Library";
        private const string TrophyStatuary = "2F Trophy Room / Statuary Room";

        override
            public string[] GetAsArray() {
            return new[] {LivingLibrary, DiningKitchen, AviatorGames, TrophyStatuary};
        }
    }
}