using System.Configuration;
using System.IO;
using System.Windows.Forms;
using RainbowCaster.Properties;

namespace RainbowCaster {
    
    public class OBSDynamicTextEditor {

        private string BaseDir = "";
        
        public OBSDynamicTextEditor(string BaseDir) {
            this.BaseDir = BaseDir;
        }

        public bool CreateEmtpyTextFiles() {
            string FullPath = Path.Combine(BaseDir, FileName.MatchTitle);
            if (!Directory.Exists(BaseDir)) {
                return false;
            }
            
            if (!File.Exists(FullPath)) {
                string[] AllFiles = FileName.GetAllFileNames();
            
                foreach (string s in AllFiles) {
                    WriteToFile(s, "");
                }
            }

            SetContent(FileName.ScoreBlue, "0");
            SetContent(FileName.ScoreOrange, "0");

            return true;
        }

        public void SetContent(string FileName, string Content) {
            WriteToFile(FileName, Content);
        }

        private void WriteToFile(string SpecificFileName, string Content) {
            string FullPath = Path.Combine(BaseDir, SpecificFileName);
            try {
                File.WriteAllText(FullPath, Content);
            }
            catch (IOException) {
                //ToDo Error Handling
            }
        }
        
        public class FileName {

            public const string MatchTitle = "MatchTitle.txt";
            public const string MatchSubTitle = "MatchSubTitle.txt";
            public const string NextMap = "NextMap.txt";
            public const string ScoreBlue = "ScoreBlue.txt";
            public const string ScoreOrange = "ScoreOrange.txt";

            public static string[] GetAllFileNames() {
                return new[] {MatchTitle, MatchSubTitle, NextMap, ScoreBlue, ScoreOrange};
            }
            
        }
        
    }
    
}